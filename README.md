INSTALASI di ubuntu 18.04

### Instalasi library
	sudo apt install php7.2-cli
	sudo apt install php-7.2
	sudo apt install php-dev libmcrypt-dev php-pear
	sudo pecl channel-update pecl.php.net
	sudo pecl install mcrypt-1.0.1


### konfigurasi file php 
	sudo vi /etc/php/7.2/cli/php.ini 


### edit konfigurasi php. aktifkan
	display_errors = On
	extension=mcrypt.so

### untuk mengecek apakah sudah jalan modulnya
	php -i |grep mcrypt



### menjalankan web service
	cd ~/
	mkdir webroot
	cd webroot
	php -S localhost:8000 ~/webroot

buka browser dan jalankan alamat server nya dengan port 8000
contoh localhost:8000. bisa dengan IP komputer