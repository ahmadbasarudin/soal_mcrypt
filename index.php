<?php

	function enkripsi(String $_plaintext)
	{
		//load konfigurasi enkripsi
		$encrypt_config 	= require("config.mcrypt.php"); 
		//untuk enkripsi:
		$encrypted 	= 	mcrypt_encrypt 	(
											constant($encrypt_config['ALOGORITMA']), 
											$encrypt_config['KEY'], 
											$_plaintext, 
											constant($encrypt_config['MODE'])
										);
		return $encrypted;
	}

	//mengembalikan nilai string berformat plaintext
	function dekripsi(String $_chipertext)
	{
		//load konfigurasi enkripsi
		$encrypt_config 	= require("config.mcrypt.php"); 
		//untuk dekripsi:
		$decrypted = mcrypt_decrypt 	(
											constant($encrypt_config['ALOGORITMA']), 
											$encrypt_config['KEY'], 
											$_chipertext, 
											constant($encrypt_config['MODE'])
										);
		return $decrypted;


	}

	/*-----------------------------------------------------------------------
	 *-----------------------------------------------------------------------
	 * MAIN CODING
	 *-----------------------------------------------------------------------
	 */


	//pat file konfigurasi yang akan dienkripsi	
	$db_config 			= require("config.db.php"); 

	//konfigurasi dijadikan plaintext dengan format json
	//data ini akan dijadikan bahan enkripsi
	$json_data = json_encode($db_config);


	/*-----------------------------------------------------------------------
	 * mulai enkripsi
	 */
	$encrypted = enkripsi($json_data);
	//simpan kedalam cookies
	setcookie('konfigurasi_enkripsi', $encrypted, strtotime('+1 days'), '/');


	/*-----------------------------------------------------------------------
	 * mulai dekripsi
	 */
	if(isset($_COOKIE['konfigurasi_enkripsi']))
	{
		$chiper = $_COOKIE['konfigurasi_enkripsi'];
		$plaintext = dekripsi($chiper);


		//test output
		//menghilangkan karakter dibelakang
		$actual_data = rtrim($plaintext, "\0");
		echo "ini plaintextnya:".$actual_data."<br />";
		//parsing json menjadi array
		$data = json_decode($actual_data,true);
		echo "data konfigurasi:";
		var_dump($data);
	}



